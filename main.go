package main

import (
    "bitbucket.org/OskarWickstrom/ssego"
    "encoding/json"
    "fmt"
    "github.com/gorilla/mux"
    "io/ioutil"
    "net/http"
    "os"
    "strings"
)

type Message struct {
    Author string
    Body   string
}

func newMessagePublisher(broker ssego.Broker) http.HandlerFunc {
    return func(w http.ResponseWriter, r *http.Request) {
        data, err := ioutil.ReadAll(r.Body)

        if err != nil {
            http.Error(w, "failed to handle request", http.StatusInternalServerError)
            return
        }
        msg := Message{}
        err = json.Unmarshal(data, &msg)

        if err != nil {
            http.Error(w, "failed to deserialize message", http.StatusBadRequest)
            return
        }

        if strings.TrimSpace(msg.Author) == "" {
            http.Error(w, "author must be set", http.StatusBadRequest)
            return
        }

        if strings.TrimSpace(msg.Body) == "" {
            http.Error(w, "message body can't be empty", http.StatusBadRequest)
            return
        }

        fmt.Printf("%s: %s\n", msg.Author, msg.Body)
        data, err = json.Marshal(msg)

        if err != nil {
            http.Error(w, "failed to serialize message", http.StatusInternalServerError)
            return
        }

        broker.Publish(data) // send the JSON message to all subscribers
    }
}
const (
    url = "localhost:8888"
)

func main() {
    broker := ssego.NewBroker()

    if len(os.Args) < 2 {
        fmt.Fprintf(os.Stderr, "path to static directory must be provided\n")
        os.Exit(1)
    }
    staticDir := os.Args[1]

    r := mux.NewRouter()
    r.Handle("/messages", newMessagePublisher(broker)).Methods("POST")
    r.Handle("/messages", ssego.NewEventSource(broker)).Methods("GET")

    http.Handle("/messages", r)
    http.Handle("/", http.FileServer(http.Dir(staticDir)))

    fmt.Println("listening at", url)

    err := http.ListenAndServe(url, nil)
    if err != nil {
        panic(err)
    }
}
