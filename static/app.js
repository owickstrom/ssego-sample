(function () {
    'use strict';
    var chat = angular.module('chat', []);

    chat.config(function ($routeProvider, $locationProvider) {
        $locationProvider.html5Mode(false);

        $routeProvider
            .when('/', {controller: 'ChatController'});
    });

    chat.controller('ChatController', function ($scope, $http, $location, $routeParams) {
        $scope.history = [];

        $scope.sendMessage = function () {
            if (!$scope.currentMessage) {
                return;
            }

            $http.post("/messages", {author: $scope.user, body: $scope.currentMessage})
                .success(function () {
                    $scope.currentMessage = "";
                })
                .error(function (err) {
                    console.log(err);
                    alert("Failed to send message, try again.");
                });
        };


        $scope.setupEventSource = function () {
            var source = new EventSource('/messages');
            source.onopen = function (event) {
                console.log("eventsource connection open");
            };
            source.onerror = function (event) {
                if (event.target.readyState === 0) {
                    console.log("reconnecting to eventsource");
                } else {
                    console.log("eventsource error");
                }
            };
            source.onmessage = function (event) {
                var message = JSON.parse(event.data);
                $scope.history.push(message);
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
                $(window).scrollTop($(document).height());
            };
        };
        while (!$scope.user) {
            $scope.user = prompt("What's your name?", "");
        }
        $scope.setupEventSource();
    });
}());